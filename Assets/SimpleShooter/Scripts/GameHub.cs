﻿namespace Shooter
{
	using System;
	using UnityEngine;
	using Shooter.Classic;
	
	[Serializable]
	public struct Player
	{
		public Animator Animator;
		public Transform Transform;
		public PlayerHealth Health;
		public PlayerMovement Movement;
		public PlayerShooting Shoot;
	}
	
	public partial class GameHub : MonoBehaviour
	{
		[SerializeField]
		private Player _player;
		
		public Player Player
		{
			get
			{
				return _player;
			}
		}
		
		public static GameHub Instance;
		
		private void Awake()
		{
			if ( Instance == null )
			{
				Instance = this;
			}
			else if ( Instance != this )
			{
				Destroy( gameObject );
			}
		}
	}
}