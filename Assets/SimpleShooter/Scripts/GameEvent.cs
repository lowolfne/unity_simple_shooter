﻿namespace Shooter.Events
{
	using System;
	
	public static class GameEvent
	{
		private static event Action<int> _onLevelChange;
		
		public static event Action<int> OnLevelChange
		{
			add
			{
				_onLevelChange += value;
			}
			remove
			{
				_onLevelChange -= value;
			}
		}
		
		public static void TriggerLevelChange( int level )
		{
			if ( _onLevelChange != null )
			{
				_onLevelChange.Invoke( level );
			}
		}
		
		public static void Clear()
		{
			_onLevelChange = null;
		}
	}
}