﻿namespace Shooter
{
	using UnityEngine;
	
	public class GameData : MonoBehaviour
	{
		private static GameData _instance;
		public static GameData Instance
		{
			get
			{
				return _instance;
			}
		}
		
		[SerializeField]
		private int _score;
		
		public int Score
		{
			get
			{
				return _score;
			}
			set
			{
				_score = value;
			}
		}
		
		private void Awake()
		{
			if ( _instance == null )
			{
				_instance = this;
			}
			else if ( _instance != this )
			{
				Destroy( gameObject );
			}
		}
		
		private void Reset()
		{
			Score = 0;
		}
	}
}