﻿namespace Shooter.Events
{
	using System;
	
	public static class PlayerEvent
	{
		private static event Action<int> _onPlayerDamage;
		
		public static event Action<int> OnPlayerDamage
		{
			add
			{
				_onPlayerDamage += value;
			}
			remove
			{
				_onPlayerDamage -= value;
			}
		}
		
		public static void TriggerPlayerDamage( int damageAmount )
		{
			if ( _onPlayerDamage != null )
			{
				_onPlayerDamage.Invoke( damageAmount );
			}
		}
		
		private static event Action<int> _onPlayerScore;
		
		public static event Action<int> OnPlayerScore
		{
			add
			{
				_onPlayerScore += value;
			}
			remove
			{
				_onPlayerScore -= value;
			}
		}
		
		public static void TriggerPlayerScore( int score )
		{
			if ( _onPlayerScore != null )
			{
				_onPlayerScore.Invoke( score );
			}
		}
		
		private static event Action _onPlayerDead;
		
		public static event Action OnPlayerDead
		{
			add
			{
				_onPlayerDead += value;
			}
			remove
			{
				_onPlayerDead -= value;
			}
		}
		
		public static void TriggerPlayerDead()
		{
			if ( _onPlayerDead != null )
			{
				_onPlayerDead.Invoke();
			}
		}
		
		public static void Clear()
		{
			_onPlayerDamage = null;
			_onPlayerScore = null;
			_onPlayerDead = null;
		}
	}
}

