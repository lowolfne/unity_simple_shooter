﻿namespace Shooter.Classic
{
	using UnityEngine;
	using UnityEngine.UI;
	using Shooter.Events;
	
	public class PlayerHealth : MonoBehaviour
	{
		[SerializeField]
		private int _initialHealth = 0;
		
		[SerializeField]
		private int _currentHealth = 0;
		
		public int Health
		{
			get
			{
				return _currentHealth;
			}
		}
		
		[SerializeField]
		private AudioClip _deathClip = null;
		
		[SerializeField]
		private Animator _animator = null;
		
		[SerializeField]
		private AudioSource _playerAudio = null;
		
		[SerializeField]
		private PlayerShooting _playerShooting = null;
		
		private bool _isDead = false;
		
		private void Start()
		{
			_currentHealth = _initialHealth;
			
			PlayerEvent.OnPlayerDamage -= SetDamage;
			PlayerEvent.OnPlayerDamage += SetDamage;
		}
		
		
		
		private void SetDamage( int amount )
		{
			_currentHealth -= amount;
			
			if ( _playerAudio != null )
			{
				_playerAudio.Play();
			}
			
			if ( _currentHealth <= 0 && !_isDead )
			{
				SetDeath();
			}
		}
		
		private void SetDeath()
		{
			_isDead = true;
			
			if ( _playerShooting != null )
			{
				_playerShooting.DisableEffects();
				_playerShooting.enabled = false;
			}
			
			if ( _animator != null )
			{
				_animator.SetTrigger( "Die" );
			}
			
			if ( _playerAudio != null )
			{
				_playerAudio.clip = _deathClip;
				_playerAudio.Play();
			}
			
			PlayerEvent.TriggerPlayerDead();
		}
		
		public void RestartLevel()
		{
			// do nothing for now
			//SceneManager.LoadScene(0);
		}
	}
}