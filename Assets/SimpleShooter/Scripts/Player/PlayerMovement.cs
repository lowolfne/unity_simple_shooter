﻿namespace Shooter.Classic
{
	using UnityEngine;
	
	public class PlayerMovement : MonoBehaviour
	{
		[SerializeField]
		private float _speed = 6f;
		
		[SerializeField]
		private Animator _animator = null;
		
		[SerializeField]
		private Rigidbody _rigidBody = null;
		
		[SerializeField]
		private float _cameraRayLength = 100f;
		
		[SerializeField]
		private PlayerHealth _playerHealth = null;
		
		private int _floorMask;
		private Vector3 _movement;
		
		private void Start()
		{
			_floorMask = LayerMask.GetMask( "Floor" );
		}
		
		private void FixedUpdate()
		{
			if ( _playerHealth.Health <= 0 )
			{
				return;
			}
			
			float h = Input.GetAxisRaw( "Horizontal" );
			float v = Input.GetAxisRaw( "Vertical" );
			
			Move( h, v );
			Turning();
			SetAnimation( h, v );
		}
		
		private void Move( float h, float v )
		{
			if ( _rigidBody == null )
			{
				return;
			}
			
			_movement.Set( h, 0, v );
			_movement = _movement.normalized * _speed * Time.deltaTime;
			
			_rigidBody.MovePosition( transform.position + _movement );
		}
		
		private void Turning()
		{
			if ( _rigidBody == null )
			{
				return;
			}
			
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit hit;
			
			if ( Physics.Raycast( ray, out hit, _cameraRayLength, _floorMask ) )
			{
				Vector3 playerToMouse = hit.point - transform.position;
				playerToMouse.y = 0f;
				
				Quaternion rotation = Quaternion.LookRotation( playerToMouse );
				_rigidBody.MoveRotation( rotation );
			}
		}
		
		private void SetAnimation( float h, float v )
		{
			if ( _animator == null )
			{
				return;
			}
			
			bool isWalking = h != 0f || v != 0f;
			_animator.SetBool( "IsWalking", isWalking );
		}
	}
	
}
