﻿namespace Shooter.Classic
{
	using UnityEngine;
	using Shooter;
	using Shooter.Events;
	
	public class PlayerShooting : MonoBehaviour
	{
		[SerializeField]
		private int _damagePerShot = 20;
		
		[SerializeField]
		private float _timeBetweenBullets = 0.15f;
		
		[SerializeField, Range( 0, 100 )]
		private float _range = 100f;
		
		[SerializeField]
		private int _shootableMask;
		
		[SerializeField]
		private ParticleSystem _gunParticles = null;
		
		[SerializeField]
		private LineRenderer _gunLineRenderer = null;
		
		[SerializeField]
		private AudioSource _gunAudioSource = null;
		
		[SerializeField]
		private GameObject _gunBarrelGameObject = null;
		
		[SerializeField]
		private Light _gunLight = null;
		
		[SerializeField, Range( 0, 1 )]
		private float _effectsDisplayTime = 0.2f;
		
		private float _timer = 0;
		
		private Ray _shootRay;
		private RaycastHit _shootHit;
		
		private void Start()
		{
			_shootableMask = LayerMask.GetMask( "Shootable" );
		}
		
		private void Update()
		{
			_timer += Time.deltaTime;
			
			if ( Input.GetButton( "Fire1" ) && _timer >= _timeBetweenBullets )
			{
				Shoot();
			}
			
			if ( _timer >= _timeBetweenBullets * _effectsDisplayTime )
			{
				DisableEffects();
			}
		}
		
		public void DisableEffects()
		{
			if ( _gunLineRenderer != null )
			{
				_gunLineRenderer.enabled = false;
			}
			
			if ( _gunLight != null )
			{
				_gunLight.enabled = false;
			}
		}
		
		private void Shoot()
		{
			if ( GameHub.Instance.Player.Health != null
			     && GameHub.Instance.Player.Health.Health <= 0 )
			{
				return;
			}
			
			if ( _gunBarrelGameObject == null )
			{
				return;
			}
			
			_timer = 0f;
			
			if ( _gunAudioSource != null )
			{
				_gunAudioSource.Play();
			}
			
			if ( _gunLight != null )
			{
				_gunLight.enabled = true;
			}
			
			if ( _gunParticles != null )
			{
				_gunParticles.Stop();
				_gunParticles.Play();
			}
			
			if ( _gunLineRenderer != null )
			{
				_gunLineRenderer.enabled = true;
				_gunLineRenderer.SetPosition( 0, _gunBarrelGameObject.transform.position );
			}
			
			_shootRay.origin = _gunBarrelGameObject.transform.position;
			_shootRay.direction = _gunBarrelGameObject.transform.forward;
			
			if ( Physics.Raycast( _shootRay, out _shootHit, _range, _shootableMask ) )
			{
				EnemyEvent.TriggerEnemyDamage( _shootHit, _damagePerShot, _shootHit.point );
				
				_gunLineRenderer.SetPosition( 1, _shootHit.point );
			}
			else
			{
				_gunLineRenderer.SetPosition( 1, _shootRay.origin + _shootRay.direction * _range );
			}
		}
	}
	
}
