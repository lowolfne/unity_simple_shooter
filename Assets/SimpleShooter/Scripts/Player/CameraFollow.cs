﻿using UnityEngine;
using Shooter;
public class CameraFollow : MonoBehaviour
{
	[SerializeField]
	private Transform _target = null;
	
	[SerializeField]
	private float _smoothing = 5f;
	
	private Vector3 _offset;
	
	private void Start()
	{
		_target = GameHub.Instance.Player.Transform;
		_offset = transform.position - _target.position;
	}
	
	private void FixedUpdate()
	{
		if ( _target != null )
		{
			Vector3 targetCameraPosition = _target.position + _offset;
			transform.position = Vector3.Lerp( transform.position, targetCameraPosition, _smoothing * Time.deltaTime );
		}
	}
}
