﻿using UnityEngine;
using UnityEngine.UI;

public class DamageFloatingText : MonoBehaviour
{
	[SerializeField]
	private Animator _animator;
	
	[SerializeField]
	private Text _damageText;
	
	private void Start()
	{
		if ( _animator != null )
		{
			AnimatorClipInfo[] clipInfo = _animator.GetCurrentAnimatorClipInfo( 0 );
			Destroy( gameObject, clipInfo[0].clip.length );
		}
	}
	
	public void SetText( string text )
	{
		if ( _damageText != null )
		{
			_damageText.text = text;
		}
	}
}
