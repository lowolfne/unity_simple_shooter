﻿using UnityEngine;
using UnityEngine.UI;

using Shooter;
using Shooter.Events;

public class HUDCanvas : MonoBehaviour
{
	[SerializeField]
	private Animator _animator = null;
	
	[SerializeField]
	private Slider _healthSlider = null;
	
	[SerializeField]
	private Image _damageImage = null;
	
	[SerializeField]
	private float _flashSpeed = 5f;
	
	[SerializeField]
	private Color _flashColor = new Color( 1f, 0f, 0f, 0.1f );
	
	[SerializeField]
	private Text _scoreText = null;
	
	[SerializeField]
	private Button _restartButton = null;
	
	[SerializeField]
	private Button _quitButton = null;
	
	[SerializeField]
	private int _reloadedScene = 0;
	
	private bool _isDamage = false;
	
	private void Start()
	{
		PlayerEvent.OnPlayerDamage -= OnPlayerDamage;
		PlayerEvent.OnPlayerDamage += OnPlayerDamage;
		
		PlayerEvent.OnPlayerScore -= OnPlayerScore;
		PlayerEvent.OnPlayerScore += OnPlayerScore;
		
		PlayerEvent.OnPlayerDead -= OnGameOver;
		PlayerEvent.OnPlayerDead += OnGameOver;
		
		_scoreText.text = "Score: " + 0;
		
		if ( _healthSlider != null )
		{
			_healthSlider.value = GameHub.Instance.Player.Health.Health;
		}
		
		if ( _quitButton != null )
		{
			_quitButton.onClick.AddListener( OnClickQuit );
		}
		
		if ( _restartButton != null )
		{
			_restartButton.onClick.AddListener( OnClickRestart );
		}
	}
	
	private void Update()
	{
		if ( _damageImage == null )
		{
			return;
		}
		
		if ( _isDamage )
		{
			_damageImage.color = _flashColor;
		}
		else
		{
			_damageImage.color = Color.Lerp( _damageImage.color, Color.clear, _flashSpeed * Time.deltaTime );
		}
		
		if ( _isDamage && _healthSlider != null )
		{
			_healthSlider.value = GameHub.Instance.Player.Health.Health;
		}
		
		_isDamage = false;
	}
	
	private void OnPlayerScore( int score )
	{
		if ( _scoreText != null )
		{
			_scoreText.text = "Score: " + GameData.Instance.Score;
		}
	}
	
	private void OnPlayerDamage( int damageAmount )
	{
		_isDamage = true;
	}
	
	private void OnGameOver()
	{
		if ( _animator != null && GameHub.Instance.Player.Health.Health <= 0 )
		{
			_animator.SetTrigger( "GameOver" );
		}
	}
	
	private void OnClickRestart()
	{
		if ( _animator != null )
		{
			_animator.SetTrigger( "GameOverHide" );
		}
		
		GameEvent.TriggerLevelChange( _reloadedScene );
	}
	
	private void OnClickQuit()
	{
		PlayerEvent.Clear();
		EnemyEvent.Clear();
		GameEvent.Clear();
		
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}
}
