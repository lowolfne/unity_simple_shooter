﻿using UnityEngine;
using UnityEngine.SceneManagement;

using Shooter.Events;

public class FaderCanvas : MonoBehaviour
{
	[SerializeField]
	private Animator _animator;
	
	private int _selectedScene;
	
	private void Start()
	{
		GameEvent.OnLevelChange -= FadeOut;
		GameEvent.OnLevelChange += FadeOut;
	}
	
	public void FadeOut( int level )
	{
		_selectedScene = level;
		
		if ( _animator != null )
		{
			_animator.SetTrigger( "FadeOut" );
		}
	}
	
	public void OnCompleteFadeOut()
	{
		PlayerEvent.Clear();
		EnemyEvent.Clear();
		GameEvent.Clear();
		
		SceneManager.LoadScene( _selectedScene );
	}
}
