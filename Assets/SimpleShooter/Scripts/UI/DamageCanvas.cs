﻿using UnityEngine;
using UnityEngine.UI;

using Shooter.Events;

public class DamageCanvas : MonoBehaviour
{
	[SerializeField]
	private Canvas _canvas;
	
	[SerializeField]
	private DamageFloatingText _damageFloatingText;
	
	private void Start()
	{
		EnemyEvent.OnEnemyDamage -= OnEnemyDamage;
		EnemyEvent.OnEnemyDamage += OnEnemyDamage;
		
		_damageFloatingText = Resources.Load<DamageFloatingText>( "Prefabs/UI/DamageTextContainer" );
	}
	
	private void OnEnemyDamage( RaycastHit hitObject, int damageAmount, Vector3 hitPoint )
	{
		DamageFloatingText damageFloatingText = Instantiate( _damageFloatingText );
		
		if ( damageFloatingText != null )
		{
			Vector2 screenPosition = Camera.main.WorldToScreenPoint( new Vector3( hitPoint.x, hitPoint.y, hitPoint.z ) );
			UnityEngine.Debug.Log( screenPosition );
			
			damageFloatingText.transform.SetParent( _canvas.transform, false );
			damageFloatingText.transform.position = screenPosition;
			damageFloatingText.SetText( damageAmount.ToString() );
		}
	}
}
