﻿namespace Shooter.Classic
{
	using UnityEngine;
	using UnityEngine.AI;
	
	using Shooter.Events;
	
	public class EnemyHealth : MonoBehaviour
	{
		[SerializeField]
		private int _initialHealth = 100;
		
		[SerializeField]
		private int _currentHealth;
		
		public int Health
		{
			get
			{
				return _currentHealth;
			}
		}
		
		[SerializeField, Range( 0, 10 )]
		private float _destroyTimer = 1f;
		
		[SerializeField]
		private float _sinkSpeed = 2.5f;
		
		[SerializeField]
		private int _scoreValue = 10;
		
		[SerializeField]
		private AudioClip _deathClip = null;
		
		[SerializeField]
		private Animator _animator = null;
		
		[SerializeField]
		private AudioSource _enemyAudio = null;
		
		[SerializeField]
		private ParticleSystem _hitParticles = null;
		
		[SerializeField]
		private CapsuleCollider _capsuleCollider = null;
		
		[SerializeField]
		private Rigidbody _rigidBody = null;
		
		[SerializeField]
		private NavMeshAgent _navMeshAgent = null;
		
		private bool _isDead = false;
		private bool _isSinking = false;
		
		private void Start()
		{
			EnemyEvent.OnEnemyDamage -= OnDamage;
			EnemyEvent.OnEnemyDamage += OnDamage;
			
			_currentHealth = _initialHealth;
		}
		
		private void Update()
		{
			if ( _isSinking )
			{
				transform.Translate( -Vector3.up * _sinkSpeed * Time.deltaTime );
			}
		}
		
		// called in animation event
		public void StartSinking()
		{
			_isSinking = true;
			
			if ( _navMeshAgent != null )
			{
				_navMeshAgent.enabled = false;
			}
			
			if ( _rigidBody != null )
			{
				_rigidBody.isKinematic = true;
			}
			
			GameData.Instance.Score += _scoreValue;
			PlayerEvent.TriggerPlayerScore( GameData.Instance.Score );
			
			Destroy( gameObject, _destroyTimer );
		}
		
		private void OnDamage( RaycastHit hitObject, int damageAmount, Vector3 hitPoint )
		{
			if ( hitObject.collider == null || hitObject.collider.gameObject == null || hitObject.collider.gameObject != gameObject )
			{
				return;
			}
			
			SetDamage( damageAmount, hitPoint );
		}
		
		public void SetDamage( int amount, Vector3 hitPoint )
		{
			if ( _isDead )
			{
				return;
			}
			
			if ( _enemyAudio != null )
			{
				_enemyAudio.Play();
			}
			
			_currentHealth -= amount;
			
			if ( _hitParticles != null )
			{
				_hitParticles.transform.position = hitPoint;
				_hitParticles.Play();
			}
			
			if ( _currentHealth <= 0 )
			{
				SetDeath();
			}
		}
		
		private void SetDeath()
		{
			_isDead = true;
			
			if ( _capsuleCollider != null )
			{
				_capsuleCollider.isTrigger = true;
			}
			
			if ( _animator != null )
			{
				_animator.SetTrigger( "Dead" );
			}
			
			if ( _enemyAudio != null )
			{
				_enemyAudio.clip = _deathClip;
				_enemyAudio.Play();
			}
		}
		
		private void OnDestroy()
		{
			EnemyEvent.OnEnemyDamage -= OnDamage;
		}
	}
}
