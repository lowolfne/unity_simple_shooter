﻿namespace Shooter.Classic
{
	using UnityEngine;
	using Shooter.Events;
	
	public class EnemyAttack : MonoBehaviour
	{
		[SerializeField]
		private float _timeBetweenAttacks = 0.5f;
		
		[SerializeField]
		private int _attackDamage = 10;
		
		[SerializeField]
		private Animator _animator = null;
		
		[SerializeField]
		private GameObject _player = null;
		
		[SerializeField]
		private PlayerHealth _playerHealth = null;
		
		[SerializeField]
		private EnemyHealth _enemyHealth = null;
		
		private bool _isPlayerInRange;
		private float _timer;
		
		private void Start()
		{
			_player = GameHub.Instance.Player.Transform.gameObject;
			_playerHealth = GameHub.Instance.Player.Health;
		}
		
		private void OnTriggerEnter( Collider other )
		{
			if ( other.gameObject == _player )
			{
				_isPlayerInRange = true;
			}
		}
		
		private void OnTriggerExit( Collider other )
		{
			if ( other.gameObject == _player )
			{
				_isPlayerInRange = false;
			}
		}
		
		private void Update()
		{
			_timer += Time.deltaTime;
			
			if ( _timer >= _timeBetweenAttacks && _isPlayerInRange
			     && _enemyHealth != null && _enemyHealth.Health > 0 )
			{
				Attack();
			}
			
			if ( _playerHealth != null && _playerHealth.Health <= 0 )
			{
				if ( _animator != null )
				{
					_animator.SetTrigger( "PlayerDead" );
				}
			}
		}
		
		private void Attack()
		{
			_timer = 0f;
			
			if ( _playerHealth != null && _playerHealth.Health > 0 )
			{
				PlayerEvent.TriggerPlayerDamage( _attackDamage );
			}
		}
	}
	
}