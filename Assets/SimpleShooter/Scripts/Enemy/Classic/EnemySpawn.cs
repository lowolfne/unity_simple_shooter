﻿namespace Shooter
{
	using UnityEngine;
	using Shooter.Classic;
	
	public class EnemySpawn : MonoBehaviour
	{
		[SerializeField]
		private GameObject _enemy = null;
		
		[SerializeField]
		private Transform[] _spawnPoints = null;
		
		[SerializeField, Range( 0, 10 )]
		private float _spawnTime = 3f;
		
		private void Start()
		{
			InvokeRepeating( "Spawn", _spawnTime, _spawnTime );
		}
		
		private void Spawn()
		{
			if ( _spawnPoints == null || GameHub.Instance.Player.Health.Health <= 0 )
			{
				return;
			}
			
			int spawnPointIndex = Random.Range( 0, _spawnPoints.Length );
			Transform spawnPoint = _spawnPoints[spawnPointIndex];
			
			Instantiate( _enemy, spawnPoint.position, spawnPoint.rotation );
		}
	}
}
