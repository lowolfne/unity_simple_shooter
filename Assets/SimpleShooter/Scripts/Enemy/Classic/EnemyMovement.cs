﻿namespace Shooter.Classic
{
	using UnityEngine;
	using UnityEngine.AI;
	
	public class EnemyMovement : MonoBehaviour
	{
		[SerializeField]
		private Transform _player = null;
		
		[SerializeField]
		private NavMeshAgent _navMeshAgent = null;
		
		private void Start()
		{
			_player = GameHub.Instance.Player.Transform;
		}
		
		private void Update()
		{
			if ( _player == null || _navMeshAgent == null || !_navMeshAgent.enabled )
			{
				return;
			}
			
			if ( _navMeshAgent != null && GameHub.Instance.Player.Health.Health <= 0 )
			{
				_navMeshAgent.enabled = false;
				return;
			}
			
			_navMeshAgent.SetDestination( _player.position );
		}
	}
}