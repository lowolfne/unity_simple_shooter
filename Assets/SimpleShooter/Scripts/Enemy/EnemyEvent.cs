﻿namespace Shooter.Events
{
	using System;
	using UnityEngine;
	
	public static class EnemyEvent
	{
		private static event Action<RaycastHit, int, Vector3> _onEnemyDamage;
		
		public static event Action<RaycastHit, int, Vector3> OnEnemyDamage
		{
			add
			{
				_onEnemyDamage += value;
			}
			remove
			{
				_onEnemyDamage -= value;
			}
		}
		
		public static void TriggerEnemyDamage( RaycastHit hitObject, int hitAmount, Vector3 hitPoint )
		{
			if ( _onEnemyDamage != null )
			{
				_onEnemyDamage.Invoke( hitObject, hitAmount, hitPoint );
			}
		}
		
		public static void Clear()
		{
			_onEnemyDamage = null;
		}
	}
}

